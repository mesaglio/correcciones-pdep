# Grupo CatDog´s

# Correcciones

## Consigna:
### Punto 1: Modelado de autos y trucos
1. Modelar el tipo de dato auto
	 ```haskell
    data Auto = Auto {
        nombre    :: String,
        nafta     :: Double,
        velocidad :: Double,
        enamorade :: String,
        truco     :: (Auto->Auto)
    } deriving(Show)
	```
	*Correcciones*: **ninguna**

    *Observaciones*: **ninguna**

2. Modelar los trucos deReversa, impresionar, nitro y fingirAmor.
    ```haskell
    fingirAmor  :: (Auto->Auto)
    fingirAmor auto = Auto {
        nombre       = nombre auto,
        nafta        = nafta auto,
        velocidad    = velocidad auto,
        enamorade    = "Petra",
        truco        = fingirAmor
    }
    
    deReversa   :: (Auto->Auto) 
    deReversa auto= Auto {
        nombre       = nombre auto,
        nafta        = (1000/5) + nafta auto,
        velocidad    = velocidad auto,
        enamorade    = enamorade auto,
        truco        = deReversa
    }
    
    impresionar :: (Auto->Auto)
    impresionar auto= Auto {
        nombre       = nombre auto,
        nafta        = nafta auto,
        velocidad    = velocidad auto * 2,
        enamorade    = enamorade auto,
        truco        = impresionar
    }
    
    nitro       :: (Auto->Auto)
    nitro auto= Auto {
        nombre       = nombre auto,
        nafta        = nafta auto,
        velocidad    = velocidad auto + 15,
        enamorade    = enamorade auto    ,
        truco        = nitro
    }
    ```
    *Correcciones*: **Repeticion de logica** al momento de cambiar velocidades y nafta. La funcion **fingirAmor** no recibe el string del nuevo amor. 
        En el caso actual siempre que queramos fingirAmor, el nuevo enamorado va a ser "Petra".

    *Observaciones*: **ninguna**

3. Modelar los siguientes autos:
	- RochaMcQueen que tiene 300 litros, su velocidad inicial es 0, su enamorado es Ronco y su truco es deReversa.
	- Biankerr (nuestro tanque ruso) tiene 500 litros, su velocidad inicial es 20, su enamorado es Tinch y su truco es impresionar.
	- Gushtav tiene 200 litros, su velocidad inicial es 130, su enamorada es PetiLaLinda y su truco es nitro.
	- Rodra que se olvidó de cargar nafta (tiene 0 litros), su velocidad inicial es 50, su enamorada es Taisa y su truco es fingirAmor con Petra.
    ```haskell
    rochaMcQueen= Auto{
        nombre       = "RochaMcqueen",
        nafta        = 300,
        velocidad    = 0,
        enamorade    = "Ronco",
        truco        = deReversa
    }    
    
    biankerr= Auto{
        nombre      = "Biankerr",
        nafta       = 500,
        velocidad   = 20,
        enamorade   = "Tinch",
        truco       = impresionar
    }  
    
    gushtav= Auto{
        nombre      = "Gushtav",
        nafta       = 200,
        velocidad   = 130,
        enamorade   = "PetiLaLinda",
        truco       = nitro
    }  
    
    rodra= Auto{
        nombre      = "Rodra",
        nafta       = 0,
        velocidad   = 50,
        enamorade   = "Taisa",
        truco       = fingirAmor
    }
     ```
     *Correcciones*: El modelado de rodra es incorrecto, ya que el truco es fingirAmor **con** Petra.

     *Observaciones*: **ninguna**

### Punto 2: Incrementar velocidad

La velocidad incrementa también según la cantidad de vocales de le enamorade de su auto:
-   Si tiene entre 1 y 2 letras aumenta 15 km/h.
-   Si tiene entre 3 y 4 aumenta 20 km/h.
-   Si tiene más de 4 aumenta 30 km/h.
  ```haskell
    vocalesEnPalabra :: String -> Int
    vocalesEnPalabra =  length . filter esVocal
    
    incrementarVelocidad :: Auto -> Double
    incrementarVelocidad auto 
     | vocalesEnPalabra (enamorade auto) >  4 = velocidad auto + 30
     | vocalesEnPalabra (enamorade auto) >= 3 = velocidad auto + 20
     | vocalesEnPalabra (enamorade auto) >= 1 = velocidad auto + 15
     | otherwise                              = velocidad auto
  ```
  *Observaciones*: **Repeticion de logica** al momento de cambiar las velocidades de los autos. La funcion **incrementarVelocidad** debe devolver un auto con su velocidad actualizada.
  **Repeticion de logica** al momento de obtener las vocales del enamorade de un auto.

  *Correcciones*:  **ninguna**

### Punto 3: Puede realizar truco
Agregar la posibilidad de preguntar si un auto puede realizar un truco. Esto es posible si tiene nafta en el tanque y si su velocidad es menor a 100.
  ```haskell
    puedeRealizarTruco :: Auto -> Bool
    puedeRealizarTruco auto = 
        nafta auto >0 && velocidad auto <100 
  ```
  *Correcciones*:  Realizar puedeRealizarTruco con **composicion**.
  
  *Observaciones*: **ninguna**

### Punto 4: Nuevos trucos    
-   comboLoco que es realizar deReversa con nitro.
-   queTrucazo primero cambia de enamorade y luego utiliza incrementar velocidad.
-   turbo lleva la nafta a 0 y aumenta la velocidad en la cantidad de nafta que tenía (*10).
  ```haskell
    comboLoco ::  Auto -> Auto
    comboLoco = deReversa . nitro
    
    queTrucazo :: Auto -> Auto -> Auto
    queTrucazo auto auto2 = Auto {  
        nombre      = nombre auto,
        nafta       = nafta auto,
        enamorade   = enamorade auto2 ,
        velocidad   = velocidad auto,
        truco       = deReversa
    } 
    
    turbo :: Auto -> Auto 
    turbo auto = Auto {
        nombre      = nombre auto,
        nafta       = 0 ,
        enamorade   = enamorade auto ,
        velocidad   = velocidad auto + ((nafta auto) * 10) ,
        truco       = deReversa
    }
  ```
*Observaciones*: queTrucazo debe recibir el nombre del nuevo enamorado y un auto. **NO** dos autos. **Repeticion de logica** en turbo al momento de aumentar la velocidad

*Correccion*: **ninguna**


